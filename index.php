<?php

require ('animal.php');
require ('frog.php');
require ('ape.php');

//Release 0

$object = new animal("Shaun");

echo "Animal name : $object->name<br>";
echo "Total legs : $object->legs (actually it has 4)<br> ";
echo "Type cold_blooded : $object->cold_blooded<br>";

//Release 1 frog

$kodok= new frog("Buduk");

echo "<br>Animal name : $kodok->name<br>";
echo "Total legs : $kodok->legs<br>";
echo "Type cold_blooded : $kodok->warm_blooded<br>";
$kodok->jump();


//Release 1 ape

$sungokong= new ape("Kera Sakti");

echo "<br><br>Animal name : $sungokong->name<br>";
echo "Total legs : $sungokong->legs<br>";
echo "Type cold_blooded : $sungokong->cold_blooded<br>";
$sungokong->yell();



?>